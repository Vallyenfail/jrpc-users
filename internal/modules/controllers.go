package modules

import (
	"gitlab.com/Vallyenfail/jrpc-users/internal/infrastructure/component"
	acontroller "gitlab.com/Vallyenfail/jrpc-users/internal/modules/auth/controller"
	aservice "gitlab.com/Vallyenfail/jrpc-users/internal/modules/auth/service"
	ucontroller "gitlab.com/Vallyenfail/jrpc-users/internal/modules/user/controller"
	"gitlab.com/Vallyenfail/jrpc-users/internal/modules/user/service"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(auth *aservice.AuthServiceJSONRPC, users *service.UserServiceJSONRPC, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(auth, components)
	userController := ucontroller.NewUser(users, components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
