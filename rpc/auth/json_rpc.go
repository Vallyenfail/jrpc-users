package auth

import (
	"context"
	aservice "gitlab.com/Vallyenfail/jrpc-users/internal/modules/auth/service"
	uservice "gitlab.com/Vallyenfail/jrpc-users/internal/modules/user/service"
)

// UserServiceJSONRPC представляет UserService для использования в JSON-RPC
type AuthServiceJSONRPC struct {
	authService aservice.Auther
}

func NewAuthServiceJSONRPC(authService aservice.Auther) *AuthServiceJSONRPC {
	return &AuthServiceJSONRPC{authService: authService}
}

func (a *AuthServiceJSONRPC) Register(in []interface{}, out *aservice.RegisterOut) error {
	n, field := in[0].(aservice.RegisterIn), in[1].(int)
	*out = a.authService.Register(context.Background(), n, field)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizeEmail(in aservice.AuthorizeEmailIn, out *aservice.AuthorizeOut) error {
	*out = a.authService.AuthorizeEmail(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizeRefresh(in aservice.AuthorizeRefreshIn, out *aservice.AuthorizeOut) error {
	*out = a.authService.AuthorizeRefresh(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) AuthorizePhone(in aservice.AuthorizePhoneIn, out *aservice.AuthorizeOut) error {
	*out = a.authService.AuthorizePhone(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) SendPhoneCode(in aservice.SendPhoneCodeIn, out *aservice.SendPhoneCodeOut) error {
	*out = a.authService.SendPhoneCode(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) VerifyEmail(in aservice.VerifyEmailIn, out *aservice.VerifyEmailOut) error {
	*out = a.authService.VerifyEmail(context.Background(), in)
	return nil
}

func (a *AuthServiceJSONRPC) SetUserer(in uservice.Userer) {
	a.authService.SetUserer(in)
}
